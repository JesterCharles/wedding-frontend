import React from "react";
import ReactDOM from "react-dom";
import { BrowserRouter as Router, Switch, Route, Link, Redirect } from "react-router-dom";
import LoginPage from "./components/aut-service/login-page";
import LinkPages from "./components/links";
import MessageEntry from "./components/messaging-service/messaging-entry";
import ExpenseEntry from "./components/wedding-planner/expenses-entry";
import WeddingEntry from "./components/wedding-planner/wedding-entry";
import WelcomePage from "./components/welcome";

ReactDOM.render(
    <Router>
        <Switch>
            <Route exact path="/">
                <Redirect to="/home"></Redirect>
            </Route>
            <Route path="/home">
                <WelcomePage></WelcomePage>
                <LoginPage></LoginPage>
            </Route>
            <Route path="/wedding">
                <LinkPages></LinkPages>
                <WeddingEntry></WeddingEntry>
                <ExpenseEntry></ExpenseEntry>
            </Route>
            <Route path="/chat">
                <LinkPages></LinkPages>
                <MessageEntry></MessageEntry>
            </Route>
        </Switch>
    </Router>,
    document.getElementById("root")
);
