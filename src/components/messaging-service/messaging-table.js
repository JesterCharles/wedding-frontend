export default function MessageTable(props) {
    const messagesArray = props.mes || [];
    const tRows = messagesArray.map((m) => (
        <tr>
            <td>{m.message}</td>
            <td>{m.sender}</td>
            <td>{m.reciever}</td>
            <td>{m.timestamp}</td>
        </tr>
    ));

    return (
        <table>
            <thead>
                <th>Message</th>
                <th>Sender</th>
                <th>Receiver</th>
                <th>Time</th>
            </thead>
            <tbody>{tRows}</tbody>
        </table>
    );
}
