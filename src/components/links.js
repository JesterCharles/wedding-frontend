import { BrowserRouter as Router, Switch, Route, Link, Redirect } from "react-router-dom";

import { useState } from "react";
import { useLocation } from "react-router";

export default function LinkPages() {
    const loginInfo = useLocation();
    const log = loginInfo.state.log;

    return (
        <div>
            <h5>Go to the other pages at the links below</h5>
            <Link to={{ pathname: "/wedding", state: { log } }}>
                <button>Wedding Planner</button>
            </Link>
            <Link to={{ pathname: "/chat", state: { log } }}>
                <button>Chat Log</button>
            </Link>
        </div>
    );
}
